//> using repository sonatype:snapshots
//> using dep com.wbillingsley::doctacular::0.3.0+3-50b0c032-SNAPSHOT

import com.wbillingsley.veautiful.*
import doctacular.*
import html.*
import installers.*

given marked:Markup = new Markup({ (s:String) => scalajs.js.Dynamic.global.marked.parse(s).asInstanceOf[String] })

val root = mountToBody(HTML.div("Loading..."))
//given HTMLMarkupTransformer = root.installMarked("4.2.5")

//val name = PushValue("")

given styles:StyleSuite = StyleSuite()

val borderedText = Styling(
    """
    border: 1px dashed red;
    border-radius: 5px;
    padding: 5px;
    """
).register()

case class Counter() extends DHtmlComponent {

    val toggle = stateVariable(true)

    override def render = div(
        p("The value is now ", span(^.cls := borderedText, toggle.value.toString)),
        button("Toggle it", ^.on.click --> { toggle.value = !toggle.value })
    )

}

@main def main = 
    styles.install()

    val deck = DeckBuilder(1280, 720)
        .veautifulSlide(
            h1("Hello world")
        ).withClass("center middle")
        .markdownSlides("""
        |## This is a title
        | 
        |This is some text
        |
        | * This is a bullet point
        |
        | 1. This is a numbered list
        |
        |---
        |
        |## Here's another slide
        |
        |With some more text
        |
        |
        |""".stripMargin)
        .veautifulSlide(<.div(
            <.h2("Here's our counter"),
            Counter()
        ))
        .mountToRoot(root)